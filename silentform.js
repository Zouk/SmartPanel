function processForm(e) {
    if (e.preventDefault) e.preventDefault();
    $('.glyphicon.glyphicon-floppy-disk').attr('class', 'glyphicon glyphicon-refresh');
    $.get(window.location.protocol + '//' + window.location.hostname + ':' + window.location.port + '/?change=yes&' + $('#bot').serialize(), function() {
        $('.glyphicon.glyphicon-refresh').attr('class', 'glyphicon glyphicon-floppy-disk');
        $('#toast').show();
        setTimeout(function() {
            $('#toast').fadeOut("slow")
        }, 3000)
    })

    // You must return false to prevent the default form behavior
    return false;
}

$('button.btn.btn-xs.btn-danger').click(function() {
    $.get(window.location.protocol + '//' + window.location.hostname + ':' + window.location.port + '/?change=yes&' + this.name + '=yes&' + $('#bot').serialize(), function(data) {
        $('.label.label-success').text('0');
        $('.label.label-danger').text('0');
        $('.label.label-default').text('0 wins/hr');
        $('.progress-bar').text('Winrate - NaN');
        $('.progress-bar').css('width', '0%');
        $('.label.label-primary').text('00:00:00');
        $('.glyphicon.glyphicon-refresh').attr('class', 'glyphicon glyphicon-floppy-disk');
    })
})

$('button.btn.btn-xs.btn-success').click(function() {
    $.get(window.location.protocol + '//' + window.location.hostname + ':' + window.location.port + '/?change=yes&' + this.name + '=yes&' + $('#bot').serialize());
})

var form = document.getElementById('bot');
if (form.attachEvent) {
    form.attachEvent("submit", processForm);
} else {
    form.addEventListener("submit", processForm);
}

$(function() {
    $("[name='afterarena'] option").each(function() {
        if (!$(this).val().indexOf('Arena')) {
            $(this).remove();
        }
    })
    $('.label.label-primary').text($('.label.label-primary').text().slice(0, -8))
    $('#toast').hide();

    setInterval(function() {
        $('.glyphicon.glyphicon-floppy-disk').attr('class', 'glyphicon glyphicon-refresh');
        $.get(window.location.protocol + '//' + window.location.hostname + ':' + window.location.port, function(data) {
            var results = data.match(/rank = ([0-9]*\.[0-9]).*doing">([a-zA-Z ]*)<\/span>.*(gamestatusFalse|gamestatusTrue).*theturn">(Turn [0-9]*).*myhp">([0-9]*)<\/span>.*theirhp">([0-9]*)<\/span>.*boardstate">([a-zA-Z ]*)<\/span>.*id="gold".*?([0-9]+)<\/span.*(capTrue|capFalse).*?<span class="label label-success">([0-9]*?)<\/span>.*?<span class="label label-danger">([0-9]*?)<\/span>.*?<span class="label label-default">(.*? wins\/hr)<\/span>.*?Winrate - ([0-9\.NaN]*%).*?<span class="label label-primary">(.*?)\..*<\/span>.*?<span class="label label-info">([0-9]*?ms)<\/span>/)
            $('#rank').text('Rank ' + results[1].slice(0, -2) + ' (' + results[1].slice(-1) + ' Stars)');
            $('#doing').text(results[2]);
            $('.gamestatusFalse, .gamestatusTrue').attr('class', results[3])
            $('#theturn').text(results[4]);
            $('#myhp').text(results[5]);
            $('#theirhp').text(results[6]);
            $('#boardstate').text(results[7]);
            $('#mygold').text(results[8]);
            $('.capFalse, .capTrue').attr('class', results[9] + ' glyphicon glyphicon-ok')
            $('.label.label-success').text(results[10]);
            $('.label.label-danger').text(results[11]);
            $('.label.label-default').text(results[12]);
            $('.progress-bar').text('Winrate - ' + results[13]);
            $('.progress-bar').css('width', results[13]);
            $('.label.label-primary').text(results[14]);
            $('.label.label-info').text(results[15]);
            $('.glyphicon.glyphicon-refresh').attr('class', 'glyphicon glyphicon-floppy-disk');
        })
    }, 10000)

});