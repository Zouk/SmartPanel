using SmartBot.Plugins.API;

using System;
using System.Net;
using System.Threading;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Collections.Generic;

namespace SmartBot.Plugins
{    

	[Serializable]
    public class bPluginDataContainer : PluginDataContainer
    {		
		//Init vars
        public int LocalServerPort { get; set; }
        public bPluginDataContainer()
		{
			Name = "SmartPanel";
		}        

    }

    public class bPlugin : Plugin
    {
		//Constructor
		public override void OnPluginCreated()
		{            
		}
		
		//Bot tick event
        public override void OnTick()
        {
            //Bot.Log("[PLUGIN] -> OnTick");
        }
		
		//Bot starting event
        public override void OnStarted()
        {
            //Bot.Log("[PLUGIN] -> OnStarted");				
            if (!DataContainer.Enabled)
                return;

            Init();
        }

		//Bot stopping event
        public override void OnStopped()
        {
            //Bot.Log("[PLUGIN] -> OnStopped");
        }

		//Turn begin event
        public override void OnTurnBegin()
        {
			//Bot.Log("[PLUGIN] -> OnTurnBegin");
        }

		//Turn end event
        public override void OnTurnEnd()
        {
			//Bot.Log("[PLUGIN] -> OnTurnEnd");
        }

		//Simulation event (AI calculation)
        public override void OnSimulation()
        {
			//Bot.Log("[PLUGIN] -> OnSimulation");
        }

		//Match begin event
        public override void OnGameBegin()
        {
			//Bot.Log("[PLUGIN] -> OnGameBegin");      
            _inGame = true;      
            switch(Bot.CurrentMode().ToString())
                    {
                        case "Arena_Auto":
                            _gameMode = "Arena";
                            break;
                        default:
                            _gameMode = Bot.CurrentMode().ToString();
                            break;
                    }
        }

        //Match end event
        public override void OnGameEnd()
        {
            //Bot.Log("[PLUGIN] -> OnGameEnd");  
            _inGame = false;          
            _status = "In Menus";
            if(!DataContainer.Enabled)
                return;
        }

		//gold balance changed event
        public override void OnGoldAmountChanged()
        {
			//Bot.Log("[PLUGIN] -> OnGoldAmountChanged");
        }

		//arena 12 wins or 3 losses event
        public override void OnArenaEnd()
        {
			//Bot.Log("[PLUGIN] -> OnArenaEnd");
        }

		//lethal found event (during a game)
        public override void OnLethal()
        {
			//Bot.Log("[PLUGIN] -> OnLethal");
        }

		//all quests completed event
        public override void OnAllQuestsCompleted()
        {
			//Bot.Log("[PLUGIN] -> OnAllQuestsCompleted");
        }

		//concede event
        public override void OnConcede()
        {
			//Bot.Log("[PLUGIN] -> OnConcede");
        }


        /* --------------- Plugin Methods -------------- */

        private WebServer _webServer;
        private int _serverPort = 8080;
        private int _autoRefresh = 10;
        private bool _serverStarted = false;
            public static int _myHP = 30;
            public static int _enHP = 30;
            public static int _theTurn = 0;
            public static string _aheadOnBoard = "Ahead on Board";
            public static bool _inGame = false;
            public static string _gameMode = "None";
            public static string _status = "In Menus";

		private void Init()
		{
			if(!DataContainer.Enabled)
				return;

            _serverPort = ((bPluginDataContainer)DataContainer).LocalServerPort;
            if (_serverPort == 0)
                _serverPort = 8080;

            Bot.Log("[PLUGIN] -> SmartPanel : Local server port: " + _serverPort);

            if (!_serverStarted)
            {
                _webServer = new WebServer(_serverPort, _autoRefresh);
                _webServer.Run();
                _serverStarted = true;
            }
            else
            {
                Bot.Log("[PLUGIN] -> SmartPanel : Webserver is running on IPs: " + _webServer.GetAllIPAdresses());
            }
            Bot.SetLatencySamplingRate(60000);
		}

        private class WebServer
        {
            private HttpListener _listener;
            private Func<HttpListenerRequest, string> _responderMethod;
            private int _autoRefresh = 60;

            public WebServer(int serverPort, int autoRefreshTime)
            {
                string prefix;

                _autoRefresh = autoRefreshTime;

                if (!HttpListener.IsSupported)
                    throw new NotSupportedException(
                        "Needs Windows XP SP2, Server 2003 or later.");

                if (serverPort == 0)
                    throw new ArgumentException("Missing server port");

                prefix = "http://+:" + serverPort.ToString() + "/";
                _responderMethod = MainPage;
                bool errorOccured = false;

                try
                {
                    HttpListener listener = new HttpListener();
                    listener.Prefixes.Add(prefix);
                    listener.AuthenticationSchemes = AuthenticationSchemes.Anonymous;
                    listener.Start();
                    _listener = listener;
                }
                catch
                {
                    errorOccured = true;
                    try // try to add permissions to run the webserver on the given port
                    {
                        string userName = Environment.UserName;
                        string netArgs = "http add urlacl url=" + prefix + " user=" + userName;

                        ProcessStartInfo psi = new ProcessStartInfo("netsh", netArgs);
                        psi.Verb = "runas";
                        psi.CreateNoWindow = true;
                        psi.WindowStyle = ProcessWindowStyle.Hidden;
                        psi.UseShellExecute = true;
                        Process.Start(psi).WaitForExit();
                    }
                    catch { }
                }

                if (errorOccured) // retry after adding permissions
                {
                    try
                    {
                        HttpListener listener = new HttpListener();
                        listener.Prefixes.Add(prefix);
                        listener.AuthenticationSchemes = AuthenticationSchemes.Anonymous;
                        listener.Start();
                        _listener = listener;
                    }
                    catch (Exception ex)
                    {
                        Bot.Log("[PLUGIN] -> SmartPanel : Webserver couldn't start! Check your permissions on the given server port! " + ex.Message);
                    }
                }                             
            }

            public void Run()
            {
                Bot.Log("[PLUGIN] -> SmartPanel : Webserver is running on IPs: " + GetAllIPAdresses());
                ThreadPool.QueueUserWorkItem((o) =>
                {                    
                    try
                    {
                        while (_listener.IsListening)
                        {
                            ThreadPool.QueueUserWorkItem((c) =>
                            {
                                var ctx = c as HttpListenerContext;
                                try
                                {
                                    string rstr = _responderMethod(ctx.Request);
                                    byte[] buf = Encoding.UTF8.GetBytes(rstr);
                                    ctx.Response.ContentLength64 = buf.Length;
                                    ctx.Response.OutputStream.Write(buf, 0, buf.Length);
                                }
                                catch (Exception ex)
                                {
                                    Bot.Log("[PLUGIN] -> SmartPanel : Webserver error: " + ex.Message);
                                } 
                                finally
                                {                                    
                                    ctx.Response.OutputStream.Close();
                                }
                            }, _listener.GetContext());
                        }
                    }
                    catch (Exception ex) {
                        Bot.Log("[PLUGIN] -> SmartPanel : Webserver error: " + ex.Message);
                    } 
                });
            }

            public void Stop()
            {
                _listener.Stop();
                _listener.Close();
            }

            public string MainPage(HttpListenerRequest request)
            {
                string deck = Bot.GetSelectedDecks().FirstOrDefault().Name;
                string currentDeck = Bot.CurrentDeck().Name;
                if (string.IsNullOrWhiteSpace(currentDeck))
                    currentDeck = deck;

                string mulligan = Bot.CurrentMulligan();
                string profile = Bot.CurrentProfile();                
                Bot.Mode mode = Bot.CurrentMode();

                if (request.QueryString["start"] == "yes")
                {
                    Bot.Log("[PLUGIN] -> SmartPanel : Starting bot...");
                    Bot.StartBot();
                }

                if (request.QueryString["stop"] == "yes")
                {
                    Bot.Log("[PLUGIN] -> SmartPanel : Stopping bot...");
                    Bot.StopBot();
                }

                if (request.QueryString["finish"] == "yes")
                {
                    Bot.Log("[PLUGIN] -> SmartPanel : Finishing the current game and stopping bot...");
                    Bot.Finish();
                }

                if (request.QueryString["reset"] == "yes")
                {
                    Bot.Log("[PLUGIN] -> SmartPanel : Reset Statistics");
                    Statistics.Reset();
                }

                if (request.QueryString["change"] == "yes")
                {
                    Bot.Log("[PLUGIN] -> SmartPanel : Changing settings to: " + request.QueryString["mode"] + ", " + request.QueryString["deck"] + ", " + request.QueryString["profile"] + ", " + request.QueryString["mulligan"]);                    
                    deck = request.QueryString["deck"];
                    profile = request.QueryString["profile"];
                    mulligan = request.QueryString["mulligan"];

                    switch(request.QueryString["mode"])
                    {
                        case "Ranked":
                            Bot.ChangeMode(Bot.Mode.Ranked);
                            mode = Bot.Mode.Ranked;
                            break;
                        case "Unranked":
                            Bot.ChangeMode(Bot.Mode.Unranked);
                            mode = Bot.Mode.Unranked;
                            break;
                        case "Arena":
                            Bot.ChangeMode(Bot.Mode.Arena);
                            mode = Bot.Mode.Arena;
                            break;
                        case "Arena_Auto":
                            Bot.ChangeMode(Bot.Mode.ArenaAuto);
                            mode = Bot.Mode.ArenaAuto;
                            break;
                    }

                    Bot.ChangeDeck(request.QueryString["deck"]);
                    Bot.ChangeProfile(request.QueryString["profile"]);
                    Bot.ChangeMulligan(request.QueryString["mulligan"]);
                }
                
                StringBuilder sb = new StringBuilder();

                try
                {
                    var directory = new DirectoryInfo("Logs");
                    FileInfo lastFile = directory.GetFiles()
                                 .OrderByDescending(f => f.LastWriteTime)
                                 .First();

                    using (StreamReader sr = new StreamReader(lastFile.FullName))
                    {
                        String line;
                        while ((line = sr.ReadLine()) != null)
                        {
                            sb.AppendLine(line);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Bot.Log("[PLUGIN] -> SmartPanel : Log reading error: " + ex.Message);
                    sb.AppendLine("The log file is too big!");
                }
                
                double winRatio = ((double)SmartBot.Plugins.API.Statistics.Wins / (double)(SmartBot.Plugins.API.Statistics.Wins + SmartBot.Plugins.API.Statistics.Losses)) * 100;
                winRatio = Math.Round(winRatio, 2);

                double winsPerHour = (double)SmartBot.Plugins.API.Statistics.Wins / (double)SmartBot.Plugins.API.Statistics.ElapsedTime.TotalHours;
                winsPerHour = Math.Round(winsPerHour, 2);
                
                StringBuilder decks = new StringBuilder();                
                foreach (Deck d in Bot.GetDecks())
                {
                    string selected = "";
                    if (deck != null && d.Name == deck)
                        selected = " selected";                    

                    decks.AppendLine("<option" + selected + ">" + d.Name + "</option>");
                }

                StringBuilder profiles = new StringBuilder();
                foreach (string p in Bot.GetProfiles())
                {
                    string selected = "";
                    if (p == profile)
                        selected = " selected";                    

                    profiles.AppendLine("<option" + selected + ">" + p + "</option>");
                }

                StringBuilder mulligans = new StringBuilder();
                foreach (string m in Bot.GetMulliganProfiles())
                {
                    string selected = "";
                    if (m == mulligan)
                        selected = " selected";                    

                    mulligans.AppendLine("<option" + selected + ">" + m + "</option>");
                }

                StringBuilder modes = new StringBuilder();                
                modes.AppendLine("<option" + (mode == Bot.Mode.Ranked ? " selected" : "") + ">Ranked</option>");
                modes.AppendLine("<option" + (mode == Bot.Mode.Unranked ? " selected" : "") + ">Unranked</option>");
                modes.AppendLine("<option" + (mode == Bot.Mode.Arena ? " selected" : "") + ">Arena</option>");
                modes.AppendLine("<option" + (mode == Bot.Mode.ArenaAuto ? " selected" : "") + ">Arena_Auto</option>");        

                SmartBot.Plugins.API.PlayerData pd = Bot.GetPlayerDatas();     
                if (Bot.CurrentBoard != null) {
                    if (Bot.CurrentBoard.MinionFriend.Count > Bot.CurrentBoard.MinionEnemy.Count) {
                        _aheadOnBoard = "Ahead on Board";
                    } else {
                        _aheadOnBoard = "Behind on Board";
                    }
                    _status = "Playing " + Bot.CurrentMode().ToString();
                    _theTurn = Bot.CurrentBoard.TurnCount;
                    _myHP = (Bot.CurrentBoard.HeroFriend.CurrentHealth + Bot.CurrentBoard.HeroFriend.CurrentArmor);
                    _enHP = (Bot.CurrentBoard.HeroEnemy.CurrentHealth + Bot.CurrentBoard.HeroEnemy.CurrentArmor);
                } else {
                    _status = "In Menus";
                    _gameMode = "None";
                    _theTurn = 0;
                }
                if (_enHP < 0)
                    _enHP = 0;
                if (_myHP < 0)
                    _myHP = 0;

                return string.Format(string.Join("", File.ReadAllLines("Plugins\\smartpanel\\index.html")), sb.ToString(), SmartBot.Plugins.API.Statistics.Wins, SmartBot.Plugins.API.Statistics.Losses, winRatio, winsPerHour, SmartBot.Plugins.API.Statistics.ElapsedTime, SmartBot.Plugins.API.Statistics.Gold, Bot.GetQuests().Count, Bot.CurrentMode(), Bot.GetAverageLatency(), _autoRefresh, decks.ToString(), profiles.ToString(), mulligans.ToString(), Bot.CurrentMulligan(), Bot.CurrentProfile(), pd.GetRank() + "." + pd.GetStars(), modes.ToString(), currentDeck, SmartBot.Plugins.API.Bot.IsGoldCapReached(), _status, _aheadOnBoard, _theTurn, _myHP, _enHP, (Bot.CurrentBoard != null));
            }

            public string GetAllIPAdresses()
            {
                string IPs = "";
                String strHostName = Dns.GetHostName();                
                IPAddress[] ipAdresses = Dns.GetHostAddresses(strHostName);                                                    

                foreach(IPAddress ipaddress in ipAdresses)
                {
                    string s = ipaddress.ToString();
                    if (!s.Contains(':'))
                        IPs += s + ", ";
                }

                IPs = IPs.Substring(0, IPs.Length - 2);

                return IPs;
            }            
        }
    }
}
